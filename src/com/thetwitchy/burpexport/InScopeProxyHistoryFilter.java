package com.thetwitchy.burpexport;

import burp.api.montoya.MontoyaApi;
import burp.api.montoya.http.message.requests.HttpRequest;
import burp.api.montoya.proxy.ProxyHistoryFilter;
import burp.api.montoya.proxy.ProxyHttpRequestResponse;

public class InScopeProxyHistoryFilter implements ProxyHistoryFilter {
    private MontoyaApi api;

    public InScopeProxyHistoryFilter(MontoyaApi api){
        this.api = api;
    }

    @Override
    public boolean matches(ProxyHttpRequestResponse requestResponse) {
        // Check if the given request is in scope.
        HttpRequest req = requestResponse.finalRequest();
        String reqUrl = req.url();
        if (api.scope().isInScope(reqUrl)){
            // The request is in scope.
            return true;
        }
        // Not in scope.
        return false;
    }
}
