# Burp Project Exporter
This Burp extension will export your HTTP request/response history to a JSON file for parsing with other tools. It is inspired by [this extension](https://github.com/BuffaloWill/burpsuite-project-file-parser).

## Usage
1. Install the `burp-project-export-all*.jar` file into your Burpsuite Pro instance. Shutdown Burp.
2. From your command line issue `java -jar -Djava.awt.headless=true ~/.burp/burpsuite_pro.jar --project-file=/path/to/your/project.burp export`.
3. Project will be exported to `burp-project-export.json`.

## Building
1. Issue `./gradlew fatJar` or `./gradlew.bat fatJar`, and retrieve the file from `./build/libs/`.