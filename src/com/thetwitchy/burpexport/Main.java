package com.thetwitchy.burpexport;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import burp.api.montoya.BurpExtension;
import burp.api.montoya.MontoyaApi;
import burp.api.montoya.core.ByteArray;
import burp.api.montoya.proxy.ProxyHttpRequestResponse;
import burp.api.montoya.utilities.CompressionType;
import burp.api.montoya.utilities.Utilities;

@SuppressWarnings("unchecked")
public class Main implements BurpExtension {
    private int MAX_REQ_RES_SIZE_BYTES = 5000000;

    @Override
    public void initialize(MontoyaApi api) {
        api.extension().setName("Burp Project Exporter");

        // Get CLI args, we need to see if we are exporting (because otherwise this runs
        // on a normal burp startup, and we only want to run when prompted)
        List<String> cliArgs = api.burpSuite().commandLineArguments();
        boolean isExporting = false;
        for (String arg : cliArgs) {
            if (arg.toLowerCase().equals("export")) {
                isExporting = true;
            }
        }

        if (!isExporting) {
            // Do nothing, this is probably a normal Burp run.
            api.logging().logToOutput(
                    "USAGE: To run this exporter, issue the command `java -jar -Djava.awt.headless=true ~/.burp/burpsuite_pro.jar --project-file=/path/to/your/project.burp export`.");
            // Do nothing more for this entire extension.
            return;
        }

        String msg = "INFO: Starting Burp Project Exporter...";
        api.logging().logToOutput(msg);
        System.out.println(msg);

        int skippedReqResCount = 0;
        JSONObject output = new JSONObject();

        // Use this to check that the JSON you get is actually a burp project export.
        output.put("type", "burp_project_export");

        // First get all in-scope history
        List<ProxyHttpRequestResponse> inScopeHistory = api.proxy().history(new InScopeProxyHistoryFilter(api));

        JSONArray requestResponseObjs = new JSONArray();
        Utilities utils = api.utilities();
        for (ProxyHttpRequestResponse proxyReqRes : inScopeHistory) {
            JSONObject tmpReqResObj = new JSONObject();

            // Compress and b64 request and response
            ByteArray reqGzBytes = utils.compressionUtils().compress(proxyReqRes.finalRequest().toByteArray(),
                    CompressionType.GZIP);
            String reqB64Str = utils.base64Utils().encodeToString(reqGzBytes);

            ByteArray resGzBytes = utils.compressionUtils().compress(proxyReqRes.originalResponse().toByteArray(),
                    CompressionType.GZIP);
            String resB64Str = utils.base64Utils().encodeToString(resGzBytes);

            if (reqB64Str.length() > MAX_REQ_RES_SIZE_BYTES || resB64Str.length() > MAX_REQ_RES_SIZE_BYTES) {
                skippedReqResCount++;
                continue;
            }
            tmpReqResObj.put("req_gz", reqB64Str);
            tmpReqResObj.put("res_gz", resB64Str);

            // Add the request/response to the list
            requestResponseObjs.add(tmpReqResObj);
        }
        output.put("http_history", requestResponseObjs);
        // Use this to check in scripts that everything was completed correctly.
        output.put("status", "OK");

        // Write out to project file.
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("burp-project-export.json"));
            writer.write(output.toJSONString());
            writer.close();
            msg = "INFO: Finished project export to 'burp-project-export.json'.";
            api.logging().logToOutput(msg);
            System.out.println(msg);

            if (skippedReqResCount > 0) {
                msg = "INFO: Skipped " + skippedReqResCount + " requests/responses due to 5MB size limit.";
                api.logging().logToOutput(msg);
                System.out.println(msg);
            }
        } catch (IOException e) {
            String errorMsg = "ERROR: Something went wrong with writing the project to disk.";
            api.logging().logToError(errorMsg);
            System.err.println(errorMsg);
        }
        // Finally, exit (since if we get here we are definately exporting, and don't
        // need burp to run anymore).
        api.burpSuite().shutdown();
    }
}